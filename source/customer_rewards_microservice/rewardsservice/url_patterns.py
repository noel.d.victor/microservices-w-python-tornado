from handlers.rewards_handler import RewardsHandler
from handlers.customerorders_handler import CustomerOrdersHandler
from handlers.customerrewards_handler import CustomerRewardsHandler

url_patterns = [
    (r'/rewards', RewardsHandler),
    (r'/customerorders', CustomerOrdersHandler),
    (r'/customerrewards', CustomerRewardsHandler),
]
