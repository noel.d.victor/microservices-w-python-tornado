from constants import CURRENCY_POINT_CONVERSION


def currency_point_convertor(total, currency_type="USD"):
    total = float(total)
    ratio = CURRENCY_POINT_CONVERSION.get(currency_type)
    return total * ratio
