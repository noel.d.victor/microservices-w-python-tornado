from pymongo import MongoClient

import tornado.web

from settings import MONGO_DB_CONNECTION
from settings import REWARDS_MONGO_DB


class RewardServiceBaseHandler(tornado.web.RequestHandler):
    """
    Base Handler Class for RewardsService Handlers
    """

    def initialize(self):
        self.client = MongoClient(
            MONGO_DB_CONNECTION.get('name'),
            MONGO_DB_CONNECTION.get('port'),
        )
        self.rewards_db = self.client[REWARDS_MONGO_DB]
