import logging

from tornado.gen import coroutine
from tornado.escape import json_decode
from tornado.web import HTTPError

from bson.json_util import dumps as bson_dumps

from models import CustomerRewards

from .base_handler import RewardServiceBaseHandler

logger = logging.getLogger()


class CustomerOrdersHandler(RewardServiceBaseHandler):

    @coroutine
    def post(self, *args, **kwargs):
        data = json_decode(self.request.body)

        # we want email address and order_total
        if not data.get("email_address", None) or not data.get("order_total", None):
            logger.info("No email address or order total sent")
            raise HTTPError(404)

        email_address = data.get("email_address")
        order_total = data.get("order_total")
        customer = CustomerRewards.get_or_create_from_order(
            email_address,
            order_total)
        customer.save()
        customer_rewards = customer.generate_save_dict()
        self.write(bson_dumps(customer_rewards))
