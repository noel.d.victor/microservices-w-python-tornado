from pymongo import ASCENDING
from tornado.gen import coroutine
from bson.json_util import dumps as bson_dumps

from models import Rewards

from .base_handler import RewardServiceBaseHandler


class RewardsHandler(RewardServiceBaseHandler):

    @coroutine
    def get(self, *args, **kwargs):
        rewards = Rewards.objects.find({}, {"_id": 0}).sort('points', ASCENDING)
        self.write(bson_dumps(list(rewards), indent=4))
