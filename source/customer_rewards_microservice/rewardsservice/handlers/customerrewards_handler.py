import logging

from tornado.gen import coroutine
from tornado.escape import json_decode
from tornado.web import HTTPError
from bson.json_util import dumps as bson_dumps

from models import CustomerRewards

from .base_handler import RewardServiceBaseHandler

logger = logging.getLogger()


class CustomerRewardsHandler(RewardServiceBaseHandler):

    @coroutine
    def get(self, *args, **kwargs):
        rewards = list(self.rewards_db.customerrewards.find({}, {"_id": 0}))
        self.write(bson_dumps(rewards, indent=4))

    @coroutine
    def post(self, *args, **kwargs):
        data = json_decode(self.request.body)
        # we want email address
        if not data.get("email_address", None):
            logger.info("No email address sent")
            raise HTTPError(404)

        email_address = data.get("email_address")
        customer = CustomerRewards.objects.find_one(
            {"email_address": email_address}
        )
        if customer is None:
            self.write(bson_dumps({'error': "No Customer Found"}, indent=4))
        else:
            self.write(bson_dumps(customer, indent=4))
