"""
This module contains the various constants reference in various parts of the app
"""
INITIAL_REWARDS = (
    {"points": 100, "rewardName": "5% off purchase", "tier": "A"},
    {"points": 200, "rewardName": "10% off purchase", "tier": "B"},
    {"points": 300, "rewardName": "15% off purchase", "tier": "C"},
    {"points": 400, "rewardName": "20% off purchase", "tier": "D"},
    {"points": 500, "rewardName": "25% off purchase", "tier": "E"},
    {"points": 600, "rewardName": "30% off purchase", "tier": "F"},
    {"points": 700, "rewardName": "35% off purchase", "tier": "G"},
    {"points": 800, "rewardName": "40% off purchase", "tier": "H"},
    {"points": 900, "rewardName": "45% off purchase", "tier": "I"},
    {"points": 1000, "rewardName": "50% off purchase", "tier": "J"}
)

# Represents the ratio of 1 currency unit to points
CURRENCY_POINT_CONVERSION = {
    "USD": 1,
    # Guessetimating for fun
    "EUR": 1.25,
    "BTC": 10989
}
