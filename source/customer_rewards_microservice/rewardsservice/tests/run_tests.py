"""
All the app related tests should be imported here.
So its simple test call for all of them per tornado recommend spec
"""

from .test_rewards_handler import TestRewardsHandler
from .test_customerrewards_handler import TestCustomerRewardsHandler
from .test_customerorders_handler import  TestCustomerOrdersHandler
