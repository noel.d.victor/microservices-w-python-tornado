# Tornado Test Reference : http://www.tornadoweb.org/en/stable/testing.html

from tornado.testing import gen_test
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient
from tornado.escape import json_encode
from tornado.escape import json_decode

from pymongo import MongoClient

from ..settings import MONGO_DB_CONNECTION
from ..settings import REWARDS_MONGO_DB

import load_mongo_data
import load_fake_customerrewards


class TestCustomerOrdersHandler(AsyncTestCase):

    def setUp(self):
        super(TestCustomerOrdersHandler, self).setUp()
        # load up the fake data
        load_mongo_data.main()
        load_fake_customerrewards.main()

    @gen_test
    def test_post_customerorders(self):
        client = AsyncHTTPClient(self.io_loop)
        response = yield client.fetch(
            "http://localhost:7050/customerorders",
            method='POST',
            body=json_encode({
                'email_address': 'test_post_customer@fakemail.com',
                'order_total': 100
            }
            ))
        data = json_decode(response.body)
        flat_data = []
        for x in data.items():
            flat_data.extend(x)
        # a bunch of fake data is loaded in setup
        self.assertIn('test_post_customer@fakemail.com', flat_data)
        # sanity check
        self.assertNotIn("best_customer@fakemail.com", flat_data)

        client = MongoClient(MONGO_DB_CONNECTION.get('name'),
                             MONGO_DB_CONNECTION.get('port'))
        # dirty test
        client[REWARDS_MONGO_DB].customerrewards.delete_one({"email_address": 'test_post_customer@fakemail.com'})
