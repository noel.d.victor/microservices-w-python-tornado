# Tornado Test Reference : http://www.tornadoweb.org/en/stable/testing.html

from tornado.testing import gen_test
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient
from tornado.escape import json_encode

from tornado.escape import json_decode


from ..constants import INITIAL_REWARDS

import load_mongo_data
import load_fake_customerrewards

class TestCustomerRewardsHandler(AsyncTestCase):

    def setUp(self):
        super(TestCustomerRewardsHandler, self).setUp()
        #load up the fake data
        load_mongo_data.main()
        load_fake_customerrewards.main()

    @gen_test
    def test_get_customerrewards(self):
        client = AsyncHTTPClient(self.io_loop)
        response = yield client.fetch("http://localhost:7050/customerrewards"
                                      , method="GET"
                                      )
        data = json_decode(response.body)

        flat_data = []
        for x in data:
            flat_data.extend(x.values())
        # a bunch of fake data is loaded in setup
        self.assertIn("valued_customer5@fakemail.com", flat_data)
        # sanity check
        self.assertNotIn("best_customer@fakemail.com", flat_data)

    @gen_test
    def test_post_customerrewards(self):
        client = AsyncHTTPClient(self.io_loop)
        response = yield client.fetch(
            "http://localhost:7050/customerrewards",
            method='POST',
            body=json_encode({
                'email_address': 'valued_customer6@fakemail.com'}
            ))
        data = json_decode(response.body)
        flat_data = []
        for x in data.items():
            flat_data.extend(x)
        # a bunch of fake data is loaded in setup
        self.assertIn("valued_customer6@fakemail.com", flat_data)
        # sanity check
        self.assertNotIn("best_customer@fakemail.com", flat_data)

        response = yield client.fetch(
            "http://localhost:7050/customerrewards",
            method='POST',
            body=json_encode({
                'email_address': 'vsd@fakemail.com'}
            ))
        data = json_decode(response.body.decode('utf-8'))
        flat_data = []
        for x in data.items():
            flat_data.extend(x)
        # a bunch of fake data is loaded in setup
        self.assertIn("No Customer Found", flat_data)

