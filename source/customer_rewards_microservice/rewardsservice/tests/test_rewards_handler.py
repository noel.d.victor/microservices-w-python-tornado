# Tornado Test Reference : http://www.tornadoweb.org/en/stable/testing.html

from tornado.testing import gen_test
from tornado.testing import AsyncTestCase
from tornado.testing import AsyncHTTPClient
from tornado.escape import json_decode


from ..constants import INITIAL_REWARDS

import load_mongo_data

class TestRewardsHandler(AsyncTestCase):

    def setUp(self):
        super(TestRewardsHandler, self).setUp()
        # load up the fake data
        load_mongo_data.main()

    @gen_test
    def test_get_rewards(self):
        client = AsyncHTTPClient(self.io_loop)
        response = yield client.fetch(
            "http://localhost:7050/rewards"
            , method="GET"
        )
        data = json_decode(response.body)

        flat_data = []
        for x in data:
            flat_data.extend(x.values())

        for item in INITIAL_REWARDS:
            for value in item.values():
                self.assertIn(value, flat_data)
