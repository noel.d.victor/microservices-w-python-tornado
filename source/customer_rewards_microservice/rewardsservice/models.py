"""
An experimental way to handle data in somewhat similar style to django
"""

import logging

from pymongo import MongoClient
from pymongo import ASCENDING
from pymongo import DESCENDING

from settings import MONGO_DB_CONNECTION
from settings import REWARDS_MONGO_DB

logger = logging.getLogger()

from utils import currency_point_convertor


class BaseManager(object):

    def __init__(self, data_collection, model):
        self.client = MongoClient(
            MONGO_DB_CONNECTION.get('name'),
            MONGO_DB_CONNECTION.get('port'),
        )
        self.rewards_db = self.client[REWARDS_MONGO_DB]
        self.data_collection = data_collection
        self.model = model

    def find(self, *args, **kwargs):
        collection = self.rewards_db[self.data_collection]
        return collection.find(*args, **kwargs)

    def find_one(self, *args, **kwargs):
        collection = self.rewards_db[self.data_collection]
        return collection.find_one(*args, **kwargs)

    def insert(self, *args, **kwargs):
        collection = self.rewards_db[self.data_collection]
        return collection.insert(*args, **kwargs)

    def update_one(self, *args, **kwargs):
        collection = self.rewards_db[self.data_collection]
        return collection.update_one(*args, **kwargs)

    def get(self, *args, **kwargs):
        data = self.find_one(*args, **kwargs)
        return self.model(data=data)


class MetaBaseMongoObject(type):
    def __init__(cls, name, bases, attrs):
        cls.objects = cls.manager(cls.data_collection, cls)


class BaseMongoCollectionObject(object, metaclass=MetaBaseMongoObject):
    manager = BaseManager
    data_collection = None
    _id = None
    # expected from db
    expected_fields = []

    def __init__(self, data=None):
        if data:
            for key, value in data.items():
                if key in self.expected_fields:
                    setattr(self, key, value)
        for item in self.expected_fields:
            if not getattr(self, item, None):
                setattr(self, item, None)

    def generate_save_dict(self):
        data = {}
        for item in self.expected_fields:
            data[item] = getattr(self, item, None)
        return data

    def save(self, *args, **kwargs):
        logger.info("Saving %s: %s", self.__class__.__name__, self._id)
        data = self.generate_save_dict()
        if self._id is None:
            data.pop("_id")
            self.objects.insert(data)
            self._id = data['_id']
        else:
            # TODO:reeval logic , this could be a problem
            uniq_id = data.pop("_id")

            if uniq_id != self._id:
                logger.error("BaseModel Save Error: ids out of sync  %s %s",
                             self.id,
                             uniq_id
                             )
                raise Exception("BaseModel Save Error: ids out of sync")

            self.objects.update_one(
                {'_id': uniq_id}, {'$set': data})


class Rewards(BaseMongoCollectionObject):
    data_collection = 'rewards'
    expected_fields = [
        "_id",
        'points',
        "rewardName",
        "tier",
    ]


class CustomerRewards(BaseMongoCollectionObject):
    data_collection = 'customerrewards'
    expected_fields = [
        "_id",
        "email_address",
        "reward_points",
        "reward_tier",
        "reward_tier_name",
        "next_reward_tier",
        "next_reward_tier_name",
        "next_reward_tier_progress",
    ]

    current_reward = None
    next_reward = None

    @classmethod
    def create_from_order(cls, email, order_total):
        points = currency_point_convertor(order_total)
        data = {
            "email_address": email,
            "reward_points": points,
        }
        return cls(data)

    @classmethod
    def get_or_create_from_order(cls, email, order_total):
        customer = CustomerRewards.objects.find_one({"email_address": email})

        if customer is None:
            customer = CustomerRewards.create_from_order(
                email, order_total
            )
        else:
            customer = CustomerRewards.objects.get(
                {"email_address": email}
            )
            customer.update_points_order(order_total)
        return customer

    def save(self, *args, **kwargs):
        logger.info("Saving %s: %s", self.__class__.__name__, self.email_address)
        self.update_rewards_information()
        super(CustomerRewards, self).save()

    def update_points_order(self, order_total):
        self.reward_points += currency_point_convertor(order_total)

    def get_current_reward(self):
        reward = Rewards.objects.find(
            {"points": {"$lte": self.reward_points}}
        ).sort("points", DESCENDING)
        if reward.count():
            reward = list(reward)[0]
        else:
            # if the below the lowest
            reward = {
                "tier": None,
                "rewardName": None,
                "points": 0
            }
        return reward

    def get_next_reward(self):
        next_reward = Rewards.objects.find(
            {"points": {"$gt": self.reward_points}}
        ).sort("points", ASCENDING)
        if next_reward.count():
            next_reward = list(next_reward)[0]
        else:
            # if there isn't a next tier
            next_reward = {
                "tier": None,
                "rewardName": None,
                "points": 0
            }
        return next_reward

    def update_rewards_information(self):
        reward = self.get_current_reward()
        self.current_reward = reward
        self.reward_tier = reward['tier']
        self.reward_tier_name = reward['rewardName']

        next_reward = self.get_next_reward()
        self.next_reward = next_reward
        self.next_reward_tier = next_reward['tier']
        self.next_reward_tier_name = next_reward['rewardName']
        self.next_reward_tier_progress = self.calc_rewards_progress()

    def calc_rewards_progress(self):
        if self.next_reward_tier is None:
            return None
        data = round(
            (self.reward_points - self.current_reward["points"])
            / (self.next_reward['points'] - self.current_reward["points"]) * 100
        )
        return data
