#!/usr/bin/env bash

set -x
#redirect stdout and stderr to output file for easy review
#2>&1 need b/c unittest wraps around std stream
cd ..
pylint --ignore=tests,run_tests.py --rcfile=/code/.pylintrc /code/ > /code/pylint_summary.log 2>&1
