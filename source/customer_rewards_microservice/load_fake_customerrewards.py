#!/usr/bin/env python
import random
from pymongo import MongoClient

from rewardsservice.constants import INITIAL_REWARDS
from rewardsservice.settings import MONGO_DB_CONNECTION
from rewardsservice.settings import REWARDS_MONGO_DB


def main():
    """
    Quick and dirty function to generate some fake user data for testing
    """
    client = MongoClient(
        MONGO_DB_CONNECTION.get('name'),
        MONGO_DB_CONNECTION.get('port'),
    )
    db = client[REWARDS_MONGO_DB]

    print("Remove/Loading Fake Customer Rewards Data")
    db.customerrewards.delete_many({})

    # for a larger project with interconnected data, I'd probably set up factory boy
    for x in range(1, 1000):
        random_reward = random.choice(INITIAL_REWARDS)
        random_int = random.randint(1, 99)
        random_points = random_reward['points'] + random_int

        # if you hit the last reward
        if INITIAL_REWARDS.index(random_reward) + 1 == len(INITIAL_REWARDS):
            next_reward_tier = None
            next_reward_name = None
            next_rewards_tier_progress = None
        else:
            # make the data is moderately randomized but still makes sense
            next_reward_index = INITIAL_REWARDS.index(random_reward) + 1
            next_reward = INITIAL_REWARDS[next_reward_index]
            next_reward_tier = next_reward["tier"]
            next_reward_name = next_reward["rewardName"]
            next_rewards_tier_progress = round(
                (random_points -
                 random_reward["points"]) / (next_reward['points']
                                             - random_reward["points"]) * 100)

        customer_rewards = {
            "email_address": "valued_customer{}@fakemail.com".format(x),
            "reward_points": random_points,
            "reward_tier": random_reward["tier"],
            "reward_tier_name": random_reward["rewardName"],
            "next_reward_tier": next_reward_tier,
            "next_reward_tier_name": next_reward_name,
            "next_reward_tier_progress": next_rewards_tier_progress,

        }
        db.customerrewards.insert_one(
            customer_rewards,
        )

    print("Fake Customer Rewards Data loaded")


if __name__ == "__main__":
    main()
