#!/usr/bin/env python
"""
This module will import a rewards fixture one at time into the mongodb
"""
from pymongo import MongoClient

from rewardsservice.constants import INITIAL_REWARDS
from rewardsservice.settings import MONGO_DB_CONNECTION
from rewardsservice.settings import REWARDS_MONGO_DB


def main():
    client = MongoClient(
        MONGO_DB_CONNECTION.get('name'),
        MONGO_DB_CONNECTION.get('port'),
    )
    db = client[REWARDS_MONGO_DB]

    print("Removing and reloading rewards in mongo {}".format(REWARDS_MONGO_DB))
    db.rewards.delete_many({})
    for reward in INITIAL_REWARDS:
        db.rewards.insert_one(reward)
        # we don't want id added to initial reward data
        reward.pop('_id')
    print("Rewards loaded in mongo")


if __name__ == "__main__":
    main()
