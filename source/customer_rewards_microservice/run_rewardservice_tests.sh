#!/usr/bin/env bash

set -x
#redirect stdout and stderr to output file for easy review
#2>&1 need b/c unittest wraps around std stream
python -m tornado.testing rewardsservice/tests/run_tests.py > tests_output.log 2>&1
