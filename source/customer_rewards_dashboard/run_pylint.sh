#!/usr/bin/env bash

set -x
#redirect stdout and stderr to output file for easy review
#2>&1 need b/c unittest wraps around std stream
pylint --rcfile=.pylintrc rewards/ > /code/pylint_summary.log 2>&1
