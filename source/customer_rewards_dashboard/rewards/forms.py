from django import forms


class CustomerOrderForm(forms.Form):
    email_address = forms.EmailField(label='Enter Email Address')
    order_total = forms.DecimalField(label='Enter Order Total', decimal_places=2)
