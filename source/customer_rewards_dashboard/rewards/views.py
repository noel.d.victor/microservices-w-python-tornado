import logging
import requests

from django.template.response import TemplateResponse
from django.views.generic.base import TemplateView

from django.http import HttpResponseRedirect
from django.urls import reverse

from .forms import CustomerOrderForm

#TODO:refactor test handling error

class RewardsView(TemplateView):
    template_name = 'templates/index.html'

    def __init__(self, logger=logging.getLogger(__name__)):
        self.logger = logger

    def get(self, request, *args, **kwargs):
        error_codes = [404, 500]
        context = self.get_context_data(**kwargs)

        # get Rewards
        serv_rewards_response = requests.get("http://rewardsservice:7050/rewards")
        context['rewards_data'] = serv_rewards_response .json()
        if serv_rewards_response.status_code in error_codes:
            context['reward_data_error'] = 'Error Occurred. Please Try Again Later.'


        # get CustomerRewards
        serv_customerrewards_response = requests.get(
            "http://rewardsservice:7050/customerrewards"
        )
        context['customerrewards_data'] = serv_customerrewards_response.json()

        if serv_customerrewards_response.status_code in error_codes:
            context['customerreward_data_error'] = 'Error Occurred. Please Try Again Later.'

        # form setup
        form = CustomerOrderForm()
        context['order_form'] = form

        return TemplateResponse(
            request,
            self.template_name,
            context
        )

    def post(self, request, *args, **kwargs):
        error_codes = [404,500]
        context = self.get_context_data(**kwargs)

        # get Rewards
        serv_rewards_response = requests.get("http://rewardsservice:7050/rewards")
        context['rewards_data'] = serv_rewards_response.json()
        if serv_rewards_response.status_code in error_codes:
            context['reward_data_error'] = 'Error Occurred. Please Try Again Later.'

        # get CustomerRewards
        serv_customerrewards_response = requests.get(
            "http://rewardsservice:7050/customerrewards"
        )
        context['customerrewards_data'] = serv_customerrewards_response.json()

        if serv_customerrewards_response.status_code in error_codes:
            context['customerreward_data_error'] = 'Error Occurred. Please Try Again Later.'

        # form setup
        form = CustomerOrderForm(request.POST)
        context['order_form'] = form


        if form.is_valid():
            # send json
            r = requests.post("http://rewardsservice:7050/customerorders", json={
                'email_address': request.POST['email_address'],
                'order_total': request.POST['order_total']
            })
            if r.status_code in error_codes:
                context['order_form'].add_error(None, 'Error Occurred. Please Try Again Later.')
                return TemplateResponse(
                    request,
                    self.template_name,
                    context
                )


            return HttpResponseRedirect(reverse('rewards'))

        return TemplateResponse(
            request,
            self.template_name,
            context
        )
