#!/usr/bin/env bash

set -x
#redirect stdout and stderr to output file for easy review
#2>&1 need b/c unittest wraps around std stream
python manage.py test > tests_output.log 2>&1
