from django.conf.urls import include, url

urlpatterns = [
    url(r'', include('rewards.urls')),
    url(r'^rewards/', include('rewards.urls'))
]
